---
layout: post
title: Chuyện tình Ny và Trysan
date: 2018-6-26 5:00:00
categories: love-story
author: hoa
---

<div class="fck_detail width_common block_ads_connect">
<p>Ny và Trysan đến Bay Area - miền bắc California vào những ngày cuối thu để thực hiện bộ ảnh cưới mang đậm chất Việt Nam với áo dài trắng, gợi nhớ những ngày họ mới quen và yêu nhau. Đám cưới của họ sẽ được tiến hành vào tháng 6 tới tại Colorado.</p>

<p>Bộ ảnh được ê kíp của studio 102 Photography thực hiện. Các đôi uyên ương có cơ hội đến Mỹ hoặc đang sinh sống tại đây có thể nhờ tới 102 Photography ghi lại những hình ảnh đẹp của hai bạn với phong cách chụp ảnh tự nhiên, đẹp mắt.</p>

<p>&nbsp;</p>

<p><img alt="1-778077-1368129091_500x0.jpg" src="http://c0.f23.img.vnecdn.net/2012/01/30/1-778077-1368129091_500x0.jpg" /></p>

<p><img alt="2-935930-1368129091_500x0.jpg" src="http://c0.f23.img.vnecdn.net/2012/01/30/2-935930-1368129091_500x0.jpg" /></p>

<p><img alt="3-328273-1368129091_500x0.jpg" src="http://c0.f24.img.vnecdn.net/2012/01/30/3-328273-1368129091_500x0.jpg" /></p>

<p><img alt="4-762855-1368129091_500x0.jpg" src="http://c0.f24.img.vnecdn.net/2012/01/30/4-762855-1368129091_500x0.jpg" /></p>

<p><img alt="5-736224-1368129091_500x0.jpg" src="http://c0.f24.img.vnecdn.net/2012/01/30/5-736224-1368129091_500x0.jpg" /></p>

<p><img alt="6-597863-1368129091_500x0.jpg" src="http://c0.f21.img.vnecdn.net/2012/01/30/6-597863-1368129091_500x0.jpg" /></p>

<p><img alt="7-105267-1368129091_500x0.jpg" src="http://c0.f23.img.vnecdn.net/2012/01/30/7-105267-1368129091_500x0.jpg" /></p>

<p><img alt="8-911871-1368129091_500x0.jpg" src="http://c0.f21.img.vnecdn.net/2012/01/30/8-911871-1368129091_500x0.jpg" /></p>

<p><img alt="9-849870-1368129092_500x0.jpg" src="http://c0.f22.img.vnecdn.net/2012/01/30/9-849870-1368129092_500x0.jpg" /></p>

<p><img alt="10-381287-1368129092_500x0.jpg" src="http://c0.f23.img.vnecdn.net/2012/01/30/10-381287-1368129092_500x0.jpg" /></p>
</div>
